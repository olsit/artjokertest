document.querySelector("#checkPromokod .form__button").onclick = () => {
    let promokod = document.querySelector("#promokod");
    checkPromokod(promokod.value);
    promokod.value = "";
};

const checkPromokod = (promokod) => {

    let arrPromokod = [];
    let evenArrPromokod  = [];
    let oddArrPromokod  = [];
    let sumEvenArrPromokod;
    let sumOddArrPromokod;
    let checkOddPairs = false;
    let oddPairs = false;
    let checkOddPairsIncreasing = false;
    let oddPairsIncreasing = false;
    let checkEventBetweenPairs = false;

    while (promokod != 0) {
        arrPromokod.unshift(promokod % 10);
        promokod = (promokod / 10) | 0;
    };

    if (arrPromokod.length != 8) {
        alert("Промокод должен состоять из 8 цифр");
        return false;
    };

    arrPromokod.forEach((item, index) => {
        if (item % 2) {
            oddArrPromokod.push(item);
            if (arrPromokod[index - 1] % 2) {
                if (checkOddPairs && checkEventBetweenPairs) {
                    oddPairs = true;
                };
                checkOddPairs = true;
                if (arrPromokod[index - 1] < arrPromokod[index]) {
                    if (checkOddPairsIncreasing && checkEventBetweenPairs) {
                        oddPairsIncreasing = true;
                    };
                    checkOddPairsIncreasing = true;
                };
            };
        } else {
            evenArrPromokod.push(item);
            if (checkOddPairsIncreasing || checkOddPairs) {
                checkEventBetweenPairs = true;
            };
        };
    });
    if (evenArrPromokod.length === 0) {
        sumEvenArrPromokod = 0;
    } else {
        sumEvenArrPromokod = evenArrPromokod.reduce((previousValue, currentValue) => +previousValue + +currentValue);
    };

    if (oddArrPromokod.length === 0) {
        sumOddArrPromokod = 0;
    } else {
        sumOddArrPromokod = oddArrPromokod.reduce((previousValue, currentValue) => +previousValue + +currentValue);
    };

    if (oddPairsIncreasing) {
        return 2000;
    } else if (oddPairs) {
        return 1000;
    } else if (sumEvenArrPromokod >= sumOddArrPromokod ) {
        return 100;
    }

    return 0;
}
